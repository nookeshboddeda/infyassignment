import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private http:HttpClient) { }
  private subject = new Subject<any>();

  createAirline(data){
    return this.http.post('http://localhost:4000/airline/createAirline',data);
  }

  getAirlineData(){
    return this.http.get('http://localhost:4000/airline/getAirlines');
  }

  updateAirline(data){
    return this.http.post('http://localhost:4000/airline/updateAirline',data);
  }

  pageRefresh(){
    this.subject.next( );
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
}
}

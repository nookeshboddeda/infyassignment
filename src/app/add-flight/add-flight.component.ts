import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import {SharedService} from './../shared.service';
// import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-add-flight',
  templateUrl: './add-flight.component.html',
  styleUrls: ['./add-flight.component.css']
})
export class AddFlightComponent implements OnInit {
  addFlight:FormGroup;
  constructor(
    // private toastr:ToastrService,
    private sharedService:SharedService
    ) { }
  min = new Date();
  ngOnInit() {
    this.addFlight = new FormGroup({
      time: new FormControl('',Validators.required),
      airline:new FormControl('',Validators.required),
      flight_number:new FormControl('',Validators.required),
      transaction:new FormControl('',Validators.required),
      terminal:new FormControl('',Validators.required),
      gate:new FormControl('',Validators.required),
      remark:new FormControl('')
    })
  }

  Submit(){
    console.log("==============this.add",this.addFlight)
    if(!this.addFlight.valid){
      // this.toastr.error('fill fields');
    }else{
      console.log("=============>this.form",this.addFlight)
      this.sharedService.createAirline(this.addFlight.value).subscribe(d=>{
        // this.toastr.success('record saved successfully');
        this.sharedService.pageRefresh();
      })
    }
  }
  

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddDataComponent} from './add-data/add-data.component';
import {AirlineComponent} from './airline/airline.component';
import { AddFlightComponent } from './add-flight/add-flight.component';

const routes: Routes = [
  {path:'list',component:AirlineComponent,pathMatch:'full'},
  {path:'addData',component:AddDataComponent,pathMatch:'full'},
  {path:'addFlight',component:AddFlightComponent,pathMatch:'full'},
  {path:'',redirectTo:'list',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit,ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { HttpClient } from "@angular/common/http";
import {ExcelService} from './../excel.service';
import {MatSort} from '@angular/material/sort';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {AddDataComponent} from './../add-data/add-data.component';
import {EchartComponent} from './../echart/echart.component';
import {SharedService} from './../shared.service';
@Component({
  selector: 'app-airline',
  templateUrl: './airline.component.html',
  styleUrls: ['./airline.component.css']
})
export class AirlineComponent implements OnInit {
  displayedColumns: string[] = ['TIME', 'AIRLINE', 'FLIGHT_NUMBER', 'TRANSACTION','TERMINAL','GATE','REMARK','ACTION'];
  dataSource:any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private httpClient: HttpClient,private excelService:ExcelService,public dialog: MatDialog,private sharedService:SharedService) { }

  ngOnInit() {
   // sample Json format like database

    // this.httpClient.get('assets/data.json').subscribe((data:any)=>{
    //   console.log("=================data=======>",data);
    //   this.dataSource  = new MatTableDataSource<any>(data);
    //   this.dataSource.paginator = this.paginator;

    // })

    // data from db.
    this.fetchData();
    this.sharedService.getMessage().subscribe(d=>{
      this.fetchData();
    })
   
  }

  fetchData(){
    this.sharedService.getAirlineData().subscribe((data:any)=>{
      this.dataSource  = new MatTableDataSource<any>(data.results);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    console.log("====================>filter",filterValue.trim().toLowerCase());
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  downloadExcel(){
    this.excelService.exportAsExcelFile(this.dataSource.filteredData,'sample')
  }

  update(ev:any){
    this.dialog.open(AddDataComponent, {
      width: '800px',
      data: ev
    });
  }

  showChart(){
    this.dialog.open(EchartComponent, {
      width: '800px',
      data: this.dataSource.filteredData
    });
  }

}


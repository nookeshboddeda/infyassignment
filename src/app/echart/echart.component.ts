import { Component, OnInit, Inject } from '@angular/core';
import { EChartOption } from 'echarts';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as _ from 'underscore';

@Component({
  selector: 'app-echart',
  templateUrl: './echart.component.html',
  styleUrls: ['./echart.component.css']
})
export class EchartComponent implements OnInit {
  terminals: any;
  flights: any = [];
  result_arr = [];
  chartOption: EChartOption;
  constructor(
    public dialogRef: MatDialogRef<EchartComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }
   series:any=[];
  ngOnInit() {
    console.log("this.data", this.data)
    this.terminals = this.data.map(d => {
      return d.terminal;
    }).filter((item, i, ar) => {
      return ar.indexOf(item) === i && item != undefined;
    }
    );
    console.log("====================terminals", this.terminals)
    this.flights = this.data.map(d => {
      return d.airline;
    }).filter((item, i, ar) => ar.indexOf(item) === i);
    console.log("====================dataEchart", this.flights)
    this.flights.map(airline => {
      let obj = {
        name: airline,
        type: 'bar',
        data: []
      }
    this.series.push(obj);
    })

    this.terminals.forEach((terminal,index) => {
      let sum = 0;
      this.data.forEach(element=>{
       this.series.forEach(s=>{
         if(element.airline == s.name && element.terminal == terminal){
           sum = sum + 1;
            s.data[index] = sum;
         }
       })
      })
    });

   console.log("===================series===>",this.series)
    // var similar_val = _.groupBy(this.data, 'airline');
    // console.log("redCars", similar_val);


    // for (let list in similar_val) {
    //   console.log("list", similar_val[list])
    //   let sum = similar_val[list].reduce((s, f) => {
    //     f.terminal = isNaN(f.terminal) ? 0 : f.terminal
    //     return s + Number(f.terminal);
    //   }, 0);
    //   console.log("sum", sum)
    //   let temp = {
    //     name: list,
    //     type: 'bar',
    //     data: [sum]
    //   }
    //   this.result_arr.push(temp);
    // }
    console.log("result_arr", this.result_arr)

    this.chartOption = {
      title: {
        text: 'Airlines Data',
        // subtext: '数据来自网络'
      },
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow'
        }
      },
      legend: {
        data: this.flights
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: {
        type: 'value',
        name:'No of flights',
        nameLocation:'center',
        boundaryGap: [0, 0.01]
      },
      yAxis: {
        type: 'category',
        name:'Terminals',
        nameLocation:'center',
        data: this.terminals
      },
      series: this.series
    };

  }

  


}

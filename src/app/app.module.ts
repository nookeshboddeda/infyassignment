import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {SharedModule} from './shared/shared.module';
import { AirlineComponent } from './airline/airline.component';
import { AddDataComponent } from './add-data/add-data.component';
import { HttpClientModule } from '@angular/common/http';
import {ExcelService} from './excel.service';
import { NgxEchartsModule } from 'ngx-echarts';
import { EchartComponent } from './echart/echart.component';
import { AddFlightComponent } from './add-flight/add-flight.component';
import { UpdateFlightComponent } from './update-flight/update-flight.component';
import { ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { FormsModule }   from '@angular/forms';
// import { ToastrModule } from 'ngx-toastr';
@NgModule({
  declarations: [
    AppComponent,
    AirlineComponent,
    AddDataComponent,
    EchartComponent,
    AddFlightComponent,
    UpdateFlightComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    HttpClientModule,
    NgxEchartsModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    FormsModule,
    // ToastrModule.forRoot()

  ],
  providers: [
    ExcelService

  ],
  entryComponents:[EchartComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }

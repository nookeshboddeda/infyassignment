import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup,FormControl,Validators } from '@angular/forms';
// import { ToastrService } from 'ngx-toastr';
import {SharedService} from './../shared.service';
@Component({
  selector: 'app-add-data',
  templateUrl: './add-data.component.html',
  styleUrls: ['./add-data.component.css']
})
export class AddDataComponent implements OnInit {
  updateFlight:FormGroup;
  min = new Date();
  constructor(
    public dialogRef: MatDialogRef<AddDataComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    // private toastr: ToastrService,
    private sharedService:SharedService
  ) { }

  ngOnInit() {
    console.log("====================data===>",this.data)
    this.updateFlight = new FormGroup({
      time: new FormControl('',Validators.required),
      airline:new FormControl('',Validators.required),
      flight_number:new FormControl('',Validators.required),
      transaction:new FormControl('',Validators.required),
      terminal:new FormControl('',Validators.required),
      gate:new FormControl('',Validators.required),
      remark:new FormControl('')
    })
  }

  Submit(){
   
      console.log("=============>this.form",this.updateFlight.get('time'))
      this.sharedService.updateAirline({id:this.data._id,data:this.updateFlight.value}).subscribe((d:any)=>{
       console.log("================>data ===>",d)
       this.sharedService.pageRefresh();
       this.dialogRef.close();
      })
  }

  close(): void {
    this.dialogRef.close();
  }

}

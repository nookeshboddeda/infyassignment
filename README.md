# InfyAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.21.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
## paths
for client: run 'ng serve' in Project root folder(port:4200)
for server: run 'node server' in ProjectFolder/server(port:4000)
## Technologies used

 1. Angular 8
 2. Angular materials Module
 3. Bootstrap
 4. Echarts
 5. Node js 
 6. Mongo DB

## Data Fetching API to local mongoDB
 run 'http://localhost:4000/airline/insert' in browser
## routers in client Application
 for airlines list : http://localhost:4200/list
 for adding airline : http://localhost:4200/addFlight
 for excel downloading : Download button in airline list page
 for sorting use filter
 for bar chart :click on show chart
## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

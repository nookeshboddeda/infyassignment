const Q = require('q');
const airlineschema = require('./airline.schema');
const request = require('request');

function createAirline(data){
    let defered = Q.defer();
    let schema = new airlineschema(data);
    schema.save().then((err,docs)=>{
        if(err){
            defered.reject({msg:'mongoose error',error:err})
        }else{
            defered.resolve({msg:'record created successfully',results:docs})
        }
    })

    return defered.promise;
}

function insertAirlines(){
 let defered = Q.defer();
 request('https://data.sfgov.org/resource/chfu-j7tc.json',function(error,response,body){
     console.log("===================>response",typeof JSON.parse(body));
     let data = JSON.parse(body);
     airlineschema.insertMany(data,(err,docs)=>{
     if(err){
         console.log("======================error==========>",err)
             defered.reject({msg:'mongoose error',error:err});
         }else{
            defered.reject({msg:'documents successfully saved',error:false});
         }
     })
 })

 return defered.promise;
}

function getAirlineData(){
    let defered = Q.defer();
    airlineschema.find({},function(err,data){
        if(err){
            defered.reject({msg:'mongoose error',error:err});
        }else{
            defered.resolve({msg:'results found',error:false,results:data})
        }
    })

    return defered.promise;
}

function updateAirline(obj){
    let defered = Q.defer();
    console.log("================>data===========>",obj);
    airlineschema.updateOne({_id:obj.id},obj.data,function(err,data){
        if(err){
            defered.reject({msg:'mongoose error',error:err});
        }else{
            defered.resolve({msg:'record updated',error:false,results:data})
        }
    })

    return defered.promise;
}





module.exports = {createAirline,insertAirlines,getAirlineData,updateAirline}
var express = require('express');
var router = express.Router();
const airlineService = require('./airline.service');

router.get('/insert',insertData);
router.get('/getAirlines',getAirlines);
router.post('/updateAirline',updateAirline);
router.post('/createAirline',createAirline);

function insertData(req,res){
    airlineService.insertAirlines().then((error,data)=>{
        if(error){
            res.send(error);
        }else{
            res.send(data);
        }
    })
}

function getAirlines(req,res){
    airlineService.getAirlineData().then((error,data)=>{
        if(error){
            res.send(error);
        }else{
            res.send(data);
        }
    })
}

function updateAirline(req,res){
    airlineService.updateAirline(req.body).then((error,data)=>{
        if(error){
            res.send(error);
        }else{
            res.send(data);
        }
    })
}

function createAirline(req,res){
    airlineService.createAirline(req.body).then((error,data)=>{
        if(error){
            res.send(error);
        }else{
            res.send(data);
        }
    })
}

module.exports= router;
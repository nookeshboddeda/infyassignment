var mongoose = require('mongoose');

var airlineSchema = mongoose.Schema({
    time:{type:String},
    transaction:{type:String},
    terminal:{type:String},
    gate:{type:String},
    remark:{type:String},
    airline:{type:String},
    flight_number:{type:String},
}, { collection: 'airlines' });
module.exports = mongoose.model('airlines', airlineSchema);

var express = require('express');
var app = express();
var mongoose = require("mongoose");
var cors = require('cors')
const bodyParser = require('body-parser');
let db = 'mongodb://localhost/Infy';
const airline = require('./airline.controller');

mongoose.connect(db,{ useNewUrlParser: true,useFindAndModify:false, useUnifiedTopology: true }, function (err, res) {
  if (err) {
    console.log('ERROR connecting to:  ' + err);
  } else {
    console.log('Succeeded connected to: ');
  }
});
app.use(cors())
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json());
app.use('/airline',airline);
app.listen(4000)